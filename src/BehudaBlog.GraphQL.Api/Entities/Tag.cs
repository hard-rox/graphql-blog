﻿using System.Collections.Generic;
using HotChocolate;

namespace BehudaBlog.GraphQL.Api.Entities
{
    public class Tag
    {
        [GraphQLNonNullType]
        public int Id { get; set; }
        [GraphQLNonNullType]
        public string Name { get; set; }
        [GraphQLIgnore]
        public virtual ICollection<ArticleTag> Articles { get; set; }
    }
}
