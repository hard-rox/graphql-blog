﻿using System;
using System.Collections.Generic;
using HotChocolate;

namespace BehudaBlog.GraphQL.Api.Entities
{
    public class Article
    {
        [GraphQLNonNullType]
        public int Id { get; set; }
        [GraphQLNonNullType]
        public string Title { get; set; }
        [GraphQLNonNullType]
        public DateTime PublishedAt { get; set; }
        [GraphQLNonNullType]
        public string Text { get; set; }
        [GraphQLIgnore]
        public int CategoryId { get; set; }
        [GraphQLNonNullType]
        public virtual Category Category { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
        public virtual ICollection<ArticleTag> Tags { get; set; }
        [GraphQLIgnore]
        public string WrittenById { get; set; }
        [GraphQLIgnore]
        public virtual ApplicationUser WrittenBy { get; set; }

        public User Writer => new User()
        {
            UserId = this.WrittenBy.Id,
            Name = this.WrittenBy.Name
        };

        public int CommentsCount => this.Comments?.Count ?? 0;
    }
}
