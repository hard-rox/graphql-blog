﻿using System.Collections.Generic;
using HotChocolate;

namespace BehudaBlog.GraphQL.Api.Entities
{
    public class Category
    {
        [GraphQLNonNullType]
        public int Id { get; set; }
        [GraphQLNonNullType]
        public string Name { get; set; }
        public string Description { get; set; }
        [GraphQLIgnore]
        public virtual ICollection<Article> Articles { get; set; }
        public int ArticleCount => this.Articles?.Count ?? 0;
    }
}
