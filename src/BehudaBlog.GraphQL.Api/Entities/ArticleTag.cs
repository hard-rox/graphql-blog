﻿using HotChocolate;

namespace BehudaBlog.GraphQL.Api.Entities
{
    public class ArticleTag
    {
        [GraphQLIgnore]
        public int ArticleId { get; set; }
        [GraphQLIgnore]
        public Article Article { get; set; }
        [GraphQLIgnore]
        public int TagId { get; set; }
        public Tag Tag { get; set; }
    }
}
