﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;

namespace BehudaBlog.GraphQL.Api.Entities
{
    public class ApplicationUser : IdentityUser
    {
        public string Name { get; set; }

        public virtual ICollection<Article> Writings { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
    }

    public class User
    {
        public string UserId { get; set; }
        public string Name { get; set; }
    }
}
