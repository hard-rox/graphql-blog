﻿using System.Security.Claims;
using System.Threading.Tasks;
using HotChocolate;
using HotChocolate.Execution.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace BehudaBlog.GraphQL.Api.GraphQL
{
    public static class GraphQLConfigurations
    {
        public static void ConfigureGraphQL(this IServiceCollection services)
        {
            services.AddGraphQL(AppSchema.Schema, new QueryExecutionOptions()
            {
                //Defines that the execution engine shall be forced to execute serially
                //since DbContext is not thread-safe.
                ForceSerialExecution = true,
                IncludeExceptionDetails = true,
            });

            services.AddQueryRequestInterceptor((context, builder, ct) =>
            {
                if (!context.User.Identity.IsAuthenticated) return Task.CompletedTask;

                var userId = context.User.FindFirstValue(ClaimTypes.NameIdentifier);
                builder.AddProperty("currentUserId", userId);
                return Task.CompletedTask;
            });
        }
    }
}
