﻿using BehudaBlog.GraphQL.Api.Entities;
using HotChocolate;

namespace BehudaBlog.GraphQL.Api.GraphQL
{
    public static class AppSchema
    {
        public static ISchema Schema = SchemaBuilder.New()
            .AddQueryType<AppQuery>()
            .AddMutationType<AppMutation>()
            .AddType<Article>()
            .AddType<ArticleTag>()
            .AddType<Category>()
            .AddType<Comment>()
            .AddType<Tag>()
            .AddAuthorizeDirectiveType()
            .Create();
    }
}
