﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BehudaBlog.GraphQL.Api.Data;
using BehudaBlog.GraphQL.Api.Entities;
using HotChocolate;
using HotChocolate.Types;
using HotChocolate.Types.Relay;
using Microsoft.EntityFrameworkCore;

namespace BehudaBlog.GraphQL.Api.GraphQL
{
    public class AppQuery
    {
        public async Task<List<Tag>> GetTagsAsync([Service]ApplicationDbContext dbContext)
        {
            try
            {
                var tags = await dbContext.Tags.ToListAsync();
                return tags;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public async Task<List<Category>> GetCategoriesAsync([Service]ApplicationDbContext dbContext)
        {
            try
            {
                var categories = await dbContext.Categories
                    .Include(c => c.Articles)
                    .ToListAsync();
                return categories;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [UsePaging]
        //[UseSelection]
        //[UseFiltering]
        [UseSorting]
        public async Task<List<Article>> GetArticlesAsync([Service] ApplicationDbContext dbContext)
        {
            try
            {
                var articles = await dbContext.Articles
                    .Include(a => a.Category)
                    .Include(a => a.Comments)
                    .Include(a => a.Tags)
                    .ThenInclude(at => at.Tag)
                    .ToListAsync();
                return articles;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public async Task<Article> GetArticleAsync(
            int articleId,
            [Service] ApplicationDbContext dbContext)
        {
            try
            {
                var article = await dbContext.Articles
                    .Include(a => a.WrittenBy)
                    .Include(a => a.Category)
                    .Include(a => a.Comments)
                    .Include(a => a.Tags)
                    .ThenInclude(at => at.Tag)
                    .FirstOrDefaultAsync(a => a.Id == articleId);
                return article;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}
