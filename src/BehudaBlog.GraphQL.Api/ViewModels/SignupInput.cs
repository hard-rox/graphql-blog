﻿using HotChocolate;

namespace BehudaBlog.GraphQL.Api.ViewModels
{
    public class SignupInput
    {
        [GraphQLNonNullType]
        public string Name { get; set; }
        [GraphQLNonNullType]
        public string Email { get; set; }
        [GraphQLNonNullType]
        public string Password { get; set; }
        [GraphQLNonNullType]
        public string ConfirmPassword { get; set; }
    }
}
