﻿using HotChocolate;

namespace BehudaBlog.GraphQL.Api.ViewModels
{
    public class CommentInput
    {
        [GraphQLNonNullType]
        public int ArticleId { get; set; }
        [GraphQLNonNullType]
        public string CommentText { get; set; }
    }
}
