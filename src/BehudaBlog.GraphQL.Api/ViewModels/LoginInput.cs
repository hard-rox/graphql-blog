﻿using HotChocolate;

namespace BehudaBlog.GraphQL.Api.ViewModels
{
    public class LoginInput
    {
        [GraphQLNonNullType]
        public string Email { get; set; }
        [GraphQLNonNullType]
        public string Password { get; set; }
    }
}
