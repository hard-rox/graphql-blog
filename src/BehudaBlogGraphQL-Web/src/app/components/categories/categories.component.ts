import { Component, OnInit } from '@angular/core';
import { Apollo } from 'apollo-angular';
import { Category } from 'src/app/models/category';
import gql from "graphql-tag";

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class CategoriesComponent implements OnInit {

  isLoading: boolean = true;
  categories: Category[];

  constructor(
    private apollo: Apollo
  ) { }

  ngOnInit(): void {
    this.loadCategories();
  }

  private loadCategories() {
    let queryNode = gql`
    {
      categories{
        id
        name
        description
        articleCount
      }
    }`;

    this.apollo
      .query<any>({
        query: queryNode,
      }).subscribe(result => {
        console.log(result);
        this.isLoading = result.loading;
        this.categories = result.data && result.data.categories
      });
  }

}
