import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HomeComponent } from './pages/home/home.component';
import { AppRoutingModule } from './app-routing.module';
import { NavComponent } from './components/nav/nav.component';
import { PopularArticlesComponent } from './components/popular-articles/popular-articles.component';
import { CategoriesComponent } from './components/categories/categories.component';
import { TagCloudComponent } from './components/tag-cloud/tag-cloud.component';
import { ArchivesComponent } from './components/archives/archives.component';
import { LoginComponent } from './pages/login/login.component';
import { GraphQLModule } from './graphql.module';
import { HttpClientModule } from '@angular/common/http';
import { ReadArticleComponent } from './pages/read-article/read-article.component';
import { ArticleComponent } from './components/article/article.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavComponent,
    PopularArticlesComponent,
    CategoriesComponent,
    TagCloudComponent,
    ArchivesComponent,
    ArticleComponent,
    LoginComponent,
    ReadArticleComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    GraphQLModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
