export interface Category {
    id: number;
    name: string;
    description?: any;
    articleCount: number;
}