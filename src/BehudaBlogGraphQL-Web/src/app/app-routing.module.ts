import { Routes, RouterModule, ExtraOptions } from "@angular/router";
import { HomeComponent } from './pages/home/home.component';
import { NgModule } from '@angular/core';
import { ReadArticleComponent } from './pages/read-article/read-article.component';

const routes: Routes = [
    {
        path: "",
        component: HomeComponent,
        pathMatch: "full"
    },
    {
        path: "articles/:page",
        component: HomeComponent
    },
    {
        path: "article/:id",
        component: ReadArticleComponent
    }
]

const config: ExtraOptions = {
    useHash: true,
    onSameUrlNavigation: "reload"
}

@NgModule({
    imports: [RouterModule.forRoot(routes, config)],
    exports: [RouterModule]
})
export class AppRoutingModule{}